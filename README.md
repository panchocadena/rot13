Rot13 function on a Text Field, Drupal 8 module
===============================================


Manual Installation
===================

uncompress file under <drupal8-root>/modules or
<drupal8-root>/modules/custom directory


Console Installation
====================
on <drupal8-root> directory in the command line run the following command:

	
    git -C modules clone https://bitbucket.org/panchocadena/rot13
  


enable module

has no dependencies.
