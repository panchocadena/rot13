<?php

namespace Drupal\rot13\Plugin\Field\FieldFormatter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'Rot13_formatter'.
 *
 * @FieldFormatter(
 *   id = "rot13_formatter",
 *   label = @Translation("Rot13 formatter"),
 *   field_types = {
 *     "string",
 *     "text",
 *     "text_long",
 *     "text_with_summary",
 *   },
 *   edit = {
 *     "editor" = "form"
 *   },
 *   quickedit = {
 *     "editor" = "plain_text"
 *   }
 * )
 */
class rot13Formatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();

    foreach ($items as $delta => $item) {
      $elements[$delta] = array(
        '#type' => 'processed_text',
        '#text' => rot13($item->value),
        '#format' => $item->format,
        '#langcode' => $item->getLangcode(),
      );
      
    }
    return $elements;
  }
  
}
  // function implements Rot13 formatter
  function rot13($string){
   for($i=0; $i < strlen($string); $i++){
    $c = ord($string[$i]);
    if($c >= ord('n') & $c <= ord('z') | $c >= ord('N') & $c <= ord('Z')){
     $c -= 13;
    }elseif($c >= ord('a') & $c <= ord('m') | $c >= ord('A') & $c <= ord('M')){
     $c += 13;
    }
    $string[$i] = chr($c);
   }   
   return $string;
  }